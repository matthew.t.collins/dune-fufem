import sys

sys.path.insert(0, '..')

from regexptools import *
from iteratortools import *
from treetools import *



# regexp for switching one level up
up = regexpMatchFunction('^ *\{')

# regexp for switching one level down
down = regexpMatchFunction('^ *\}')



# input file
infile = file(sys.argv[1], 'r')

# HierarchyIterator switching level according to up/down method
hit = HierarchyIterator(infile, up, down)

# TreeGenerator to generate a tree
tg = TreeGenerator()

# create the tree by feeding in pairs (item, level) from the HierarchyIterator
for item in hit:
    tg.feed(item)
#    for i in range(0,item[1]):
#        sys.stdout.write('>>>>')
#    sys.stdout.write(item[0].strip()+'\n')

tg.tree.printHierarchic()

infile.close()

