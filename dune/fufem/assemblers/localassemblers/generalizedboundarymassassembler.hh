// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef GENERALIZED_BOUNDARY_MASS_ASSEMBLER_HH
#define GENERALIZED_BOUNDARY_MASS_ASSEMBLER_HH


#include <dune/common/function.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/matrix.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/fufem/staticmatrixtools.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>
//#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/referenceelementhelper.hh>

/** \brief Local Robin boundary assembler
 * 
 * \tparam GridType The grid we are assembling for
 * \tparam T Type used for the set of dofs at one node
 */
template <class GridType, class BoundaryPatchType, class TrialLocalFE, class AnsatzLocalFE, class Function, class T=Dune::FieldMatrix<typename GridType::ctype,1,1>>
class GeneralizedBoundaryMassAssembler
{
    private:
        static const int dim = GridType::dimension;
        typedef typename GridType::ctype ctype;
        static const int dimworld = GridType::dimensionworld;
        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate; 

    public:
        typedef Dune::Matrix< Dune::FieldMatrix<int,1,1> > BoolMatrix;
        typedef typename Dune::Matrix<T> LocalMatrix;
        typedef typename T::field_type field_type;
        typedef VirtualGridFunction<GridType,typename Function::RangeType> GridFunction;
        //typedef typename Dune::VirtualFunction<GlobalCoordinate, Dune::FieldVector<double,1>> Function;

        /** \brief Constructor
         * \param order The quadrature order used for numerical integration
         */
         
        GeneralizedBoundaryMassAssembler(const Function& robin, const BoundaryPatchType& boundaryPatch, int order=2) :
            robin_(robin),
            boundaryPatch_(boundaryPatch),
            order_(order)
        {}

        template<class BoundaryIterator>
        void indices (const BoundaryIterator& it, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) 
        {
          isNonZero = true;
        }
        
        template<class BoundaryIterator>
        void assemble(const BoundaryIterator& it, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename Dune::template FieldVector<ctype,dim> QuadPointPositionVector;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

            localMatrix = 0.0;

            // geometry of the boundary face
            const typename BoundaryIterator::Intersection::Geometry segmentGeometry = it->geometry();


            // get quadrature rule
            const Dune::QuadratureRule<ctype, dim-1>& quad = QuadratureRuleCache<ctype, dim-1>::rule(segmentGeometry.type(), order_, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE) );

            // store values of shape functions
            std::vector<RangeType> tFEvalues(tFE.localBasis().size());
            std::vector<RangeType> aFEvalues(aFE.localBasis().size());

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                const auto inside = it->inside();

                // get quadrature point
                const Dune::FieldVector<ctype,dim-1>& quadPos = quad[pt].position();

                // get integration factor
                const ctype integrationElement = segmentGeometry.integrationElement(quadPos);

                // position of the quadrature point within the element
                const QuadPointPositionVector elementQuadPos = it->geometryInInside().global(quadPos);

                // evaluate basis functions
                tFE.localBasis().evaluateFunction(elementQuadPos, tFEvalues);
                aFE.localBasis().evaluateFunction(elementQuadPos, aFEvalues);

                // Evaluate robin function at quadrature point. If it is a grid function use that to speed up the evaluation
                typename Function::RangeType robinVal; 
                const GridFunction* gf = dynamic_cast<const GridFunction*>(&robin_);
                if (gf and gf->isDefinedOn(inside))
                    gf->evaluateLocal(inside, elementQuadPos, robinVal);
                else
                    robin_.evaluate(segmentGeometry.global(quadPos), robinVal);
                
                // compute matrix entries
                double z = quad[pt].weight()*integrationElement;
                                
                for (size_t i=0; i<tFEvalues.size(); ++i)
                {
                    ctype zi = tFEvalues[i]*z;
                    for (size_t j = 0; j<aFEvalues.size(); ++j) {
                        typename Function::RangeType Mij = robinVal;
                        Mij *= aFEvalues[j]*zi;
                        localMatrix[i][j] += Mij;
                    }
                }
            }
            return;
        }

    private:
        const Function& robin_;
        const BoundaryPatchType& boundaryPatch_;
        // quadrature order
        const int order_;
};

#endif

