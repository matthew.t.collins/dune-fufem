#ifndef LAPLACE_ASSEMBLER_HH
#define LAPLACE_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#include "dune/fufem/quadraturerules/quadraturerulecache.hh"

#include "dune/fufem/assemblers/localoperatorassembler.hh"

/** \brief Local assembler for the Laplace problem */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1> >
class LaplaceAssembler : public LocalOperatorAssembler <GridType, TrialLocalFE, AnsatzLocalFE, T >
{
    private:
        typedef LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE ,T > Base;
        static const int dim      = GridType::dimension;
        static const int dimworld = GridType::dimensionworld;
        const int quadOrder_;

        /** \brief Construct an orthonormal basis using the Gram-Schmidt algorithm
            \param basis The input basis; each matrix row is a basis vector
         */
        static Dune::FieldMatrix<double,dim-1,dim> makeOrthonormal(const Dune::FieldMatrix<double,dim-1,dim>& basis)
        {
            Dune::FieldMatrix<double,dim-1,dim> orthonormalBasis(basis);

            for (int i=0; i<dim-1; i++) {

                orthonormalBasis[i] /= orthonormalBasis[i].two_norm();

                for (int j=0; j<i; j++)
                    // project orthonormalBasis[j] onto orthonormalBasis[i], and subtract the result from orthonormalBasis[j]
                    orthonormalBasis[j].axpy(-(orthonormalBasis[i] * orthonormalBasis[j]), orthonormalBasis[i]);

            }

            return orthonormalBasis;
        }

    public:
        typedef typename Base::Element Element;
        typedef typename Element::Geometry Geometry;
        typedef typename Geometry::JacobianInverseTransposed JacobianInverseTransposed;
        typedef typename Base::BoolMatrix BoolMatrix;
        typedef typename Base::LocalMatrix LocalMatrix;

        LaplaceAssembler() :
            quadOrder_(-1)
        {}

        DUNE_DEPRECATED_MSG("Quadrature order is now selected automatically. you don't need to specify it anymore.")
        LaplaceAssembler(int quadOrder) :
            quadOrder_(quadOrder)
        {}

        void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
        }

        template <class BoundaryIterator>
        void indices(const BoundaryIterator& it, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
        }

        /** \brief Assemble the local stiffness matrix for a given element
         */
        void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename Dune::template FieldVector<double,dim> FVdim;
            typedef typename Dune::template FieldVector<double,dimworld> FVdimworld;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == element.type());
            assert(aFE.type() == element.type());

            // check if ansatz local fe = test local fe
//            if (not Base::isSameFE(tFE, aFE))
//                DUNE_THROW(Dune::NotImplemented, "LaplaceAssembler is only implemented for ansatz space=test space!");

            int rows = localMatrix.N();
            int cols = localMatrix.M();

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localMatrix = 0.0;

            // get quadrature rule
            QuadratureRuleKey tFEquad(tFE);
            QuadratureRuleKey quadKey = tFEquad.derivative().square();
            if (quadOrder_>=0)
                quadKey.setOrder(quadOrder_);
            const Dune::template QuadratureRule<double, dim>& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

            // store gradients of shape functions and base functions
            std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
            std::vector<FVdimworld> gradients(tFE.localBasis().size());


            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const FVdim& quadPos = quad[pt].position();

                // get transposed inverse of Jacobian of transformation
                const JacobianInverseTransposed& invJacobian = geometry.jacobianInverseTransposed(quadPos);

                // get integration factor
                const double integrationElement = geometry.integrationElement(quadPos);

                // get gradients of shape functions
                tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

                // transform gradients
                for (size_t i=0; i<gradients.size(); ++i)
                    invJacobian.mv(referenceGradients[i][0], gradients[i]);

                // compute matrix entries
                double z = quad[pt].weight() * integrationElement;
                for (int i=0; i<rows; ++i)
                {
                    for (int j=i+1; j<cols; ++j)
                    {
                        double zij = (gradients[i] * gradients[j]) * z;
                        Dune::MatrixVector::addToDiagonal(localMatrix[i][j],zij);
                        Dune::MatrixVector::addToDiagonal(localMatrix[j][i],zij);
                    }
                    Dune::MatrixVector::addToDiagonal(localMatrix[i][i], (gradients[i] * gradients[i]) * z);
                }
            }
            return;
        }


        /** \brief Assemble the local stiffness matrix for a given boundary face
         */
        template <class BoundaryIterator>
        void assemble(const BoundaryIterator& it, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename Dune::template FieldVector<double,dimworld> FVdimworld;

            typedef typename BoundaryIterator::Intersection::Geometry Geometry;
            typedef typename BoundaryIterator::Intersection::Entity Entity;
            typedef typename Entity::Geometry InsideGeometry;
            typedef typename InsideGeometry::JacobianInverseTransposed JacobianInverseTransposed;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;

            Entity const inside = it->inside();

            // Make sure we got suitable shape functions
            assert(tFE.type() == inside.type());
            assert(aFE.type() == inside.type());

            // check if ansatz local fe = test local fe
            if (not Base::isSameFE(tFE, aFE))
                DUNE_THROW(Dune::NotImplemented, "LaplaceAssembler is only implemented for ansatz space=test space!");

            int rows = localMatrix.N();
            int cols = localMatrix.M();

            // get geometry and store it
            const Geometry intersectionGeometry = it->geometry();
            const InsideGeometry insideGeometry = inside.geometry();

            localMatrix = 0.0;

            // get quadrature rule
            QuadratureRuleKey tFEquad(it->type(), tFE.localBasis().order());
            QuadratureRuleKey quadKey = tFEquad.derivative().square();

            const Dune::template QuadratureRule<double, dim-1>& quad = QuadratureRuleCache<double, dim-1>::rule(quadKey);

            // store gradients of shape functions and base functions
            std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
            std::vector<FVdimworld> gradients(tFE.localBasis().size());


            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const Dune::FieldVector<double,dim-1>& quadPos = quad[pt].position();

                // get transposed inverse of Jacobian of transformation
                const JacobianInverseTransposed& invJacobian = insideGeometry.jacobianInverseTransposed(it->geometryInInside().global(quadPos));

                // get integration factor
                const double integrationElement = intersectionGeometry.integrationElement(quadPos);

                // get gradients of shape functions
                tFE.localBasis().evaluateJacobian(it->geometryInInside().global(quadPos), referenceGradients);

                // Project gradient on the inside element tangent plane, to obtain
                // the correct surface gradient
                // Step 1: get an orthonormal basis of the tangent space at the current quadrature point
                typename GridType::template Codim<1>::LocalGeometry::JacobianTransposed tangentSpaceBasis = it->geometryInInside().jacobianTransposed(quadPos);
                Dune::FieldMatrix<double,dim-1,dim> orthonormalBasis = makeOrthonormal(tangentSpaceBasis);

                // Step 2: project
                std::vector<Dune::FieldVector<double,dim> > projectedReferenceGradients(tFE.localBasis().size());
                for (size_t i=0; i<projectedReferenceGradients.size(); i++) {
                    projectedReferenceGradients[i] = 0;
                    for (size_t j=0; j<dim-1; j++)
                        projectedReferenceGradients[i].axpy(referenceGradients[i][0]*orthonormalBasis[j],orthonormalBasis[j]);
                }

                // transform gradients
                for (size_t i=0; i<gradients.size(); ++i)
                    invJacobian.mv(projectedReferenceGradients[i], gradients[i]);

                // compute matrix entries
                double z = quad[pt].weight() * integrationElement;
                for (int i=0; i<rows; ++i)
                {
                    for (int j=i+1; j<cols; ++j)
                    {
                        double zij = (gradients[i] * gradients[j]) * z;
                        Dune::MatrixVector::addToDiagonal(localMatrix[i][j],zij);
                        Dune::MatrixVector::addToDiagonal(localMatrix[j][i],zij);
                    }
                    Dune::MatrixVector::addToDiagonal(localMatrix[i][i], (gradients[i] * gradients[i]) * z);
                }
            }
            
        }

};


#endif

