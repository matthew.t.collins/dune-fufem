install(FILES
    errorfractionmarking.hh
    fractionalmarking.hh
    geometricmarking.hh
    hierarchicalestimator.hh
    hierarchicestimatorbase.hh
    refinementindicator.hh
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/fufem/estimators)
