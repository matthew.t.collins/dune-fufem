// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef DUNE_FUFEM_FUNCTIONS_VIRTUAL_GRID_FUNCTION_HH
#define DUNE_FUFEM_FUNCTIONS_VIRTUAL_GRID_FUNCTION_HH


#include <dune/common/fvector.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/version.hh>

#include <dune/grid/utility/hierarchicsearch.hh>

#include "virtualdifferentiablefunction.hh"



/**
 *  \brief Virtual interface class for grid functions
 *
 *  A general grid function that might be defined only on parts of the underlying grid (isDefinedOn).
 *  If you have a grid function defined on a GridView derive from VirtualGridViewFunction instead.
 *
 *  \tparam GridType the underlying GridType
 *  \tparam RType the type of the range space
 *
 */
template <class GridType, class RType>
class VirtualGridFunction :
  public VirtualDifferentiableFunction<typename GridType::template Codim<0>::Geometry::GlobalCoordinate, RType>
{
  protected:

    typedef VirtualDifferentiableFunction<typename GridType::template Codim<0>::Geometry::GlobalCoordinate, RType> BaseType;
    typedef VirtualGridFunction<GridType, RType> ThisType;

  public:

    typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalDomainType;
    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType RangeType;
    typedef typename BaseType::DerivativeType DerivativeType;


    //! The grid type
    typedef GridType Grid;

    //! Element type of underlying grid
    typedef typename Grid::template Codim<0>::Entity Element;

    /** \brief Constructor
     *
     *  \param grid the underlying grid
     */
    VirtualGridFunction(const Grid& grid):
      grid_(&grid)
    {}

    /**
     * \brief function evaluation in global coordinates
     *
     * Default implementation using hierarchic search and evaluateLocal
     *
     * \warning may be VERY SLOW due to hierarchic search
     *
     * \param x Argument (global coordinates) for function evaluation
     * \param y Result of function evaluation.
     */
    virtual void evaluate(const DomainType& x, RangeType& y) const
    {
      GridFunctionDefinedInfo containsinfo(*this);
      Dune::HierarchicSearch<Grid, GridFunctionDefinedInfo> hsearch(*grid_, containsinfo);

      auto e = hsearch.findEntity(x);
      evaluateLocal(e, e.geometry().local(x), y);
    }

    /** \brief evaluation of 1st derivative in global coordinates
     *
     * Default implementation using hierarchic search and evaluateDerivativeLocal
     *
     * \warning may be VERY SLOW due to hierarchical search
     *
     *  \param x point (global coordinates) at which to evaluate the derivative
     *  \param d will contain the derivative at x after return
     */
    virtual void evaluateDerivative(const DomainType& x, DerivativeType& d) const
    {
      GridFunctionDefinedInfo containsinfo(*this);
      Dune::HierarchicSearch<Grid, GridFunctionDefinedInfo> hsearch(*grid_, containsinfo);

      auto e = hsearch.findEntity(x);
      evaluateDerivativeLocal(e, e.geometry().local(x), d);
    }

    /**
     * \brief Function evaluation in local coordinates.
     *
     * \param e Evaluate in local coordinates with respect to this element.
     * \param x Argument for function evaluation in local coordinates.
     * \param y Result of function evaluation.
     */
    virtual void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const = 0;


    /** \brief evaluation of derivative in local coordinates
     *
     *  \param e Evaluate in local coordinates with respect to this element.
     *  \param x point in local coordinates at which to evaluate the derivative
     *  \param d will contain the derivative at x after return
     */
    virtual void evaluateDerivativeLocal(const Element& e, const LocalDomainType& x, DerivativeType& d) const
    {
      DUNE_THROW(Dune::NotImplemented, "Derivative not implemented");
    }

    /**
     * \brief Check whether local evaluation is possible
     *
     * \param e Return true if local evaluation is possible on this element.
     */
    virtual bool isDefinedOn(const Element& e) const = 0;

    virtual ~VirtualGridFunction() {}

    /**
     * \brief Export underlying grid
     */
    const Grid& grid() const
    {
      return *grid_;
    }

  protected:

    //! the underlying grid
    const Grid* grid_;

    /**
     *  \brief Wrapper class forwarding VirtualGridFunction::isDefinedOn(Element&) as contains(Element&)
     *
     *  Needed to plug into Dune::HierarchicSearch in default implementation of evaluate.
     */
    class GridFunctionDefinedInfo
    {
      private:
        const ThisType& gridFunction_;

      public:
        /**
         *  \brief constructor
         */
        GridFunctionDefinedInfo(const ThisType& gridFunction):
          gridFunction_(gridFunction)
        {}

        /** forwarding VirtualGridFunction::isDefinedOn(...) as contains(...)
         */
        bool contains(const Element& e) const
        {
          return gridFunction_.isDefinedOn(e);
        }
    };


    // decision/implementation of interface for returning local functions postponed

    //! Raw type of input variable in local coordinates with removed reference and constness
//    typedef typename Grid::template Codim<0>::Geometry::LocalCoordinate LocalDomainType;

    //! Type of associated local functions
//    typedef typename Dune::template VirtualFunction<LocalDomainType, RangeType> LocalFunction;

//    /**
//     * \brief Create a local representation of this function on given element.
//     *
//     * \param e The local function takes coordinates with respect to this element.
//     */
//    virtual LocalFunction local(const Element& e) const = 0;

}; // end of VirtualGridFunction class





/**
 * \brief Virtual interface class for grid functions associated to grid views
 *
 */
template <class GridViewType, class RType>
class VirtualGridViewFunction :
  public VirtualGridFunction<typename GridViewType::Grid, RType>
{
  protected:
    typedef VirtualGridFunction<typename GridViewType::Grid, RType> BaseType;
//    typedef VirtualGridViewFunction<GridViewType, RType> MyType;

  public:

    typedef typename BaseType::LocalDomainType LocalDomainType;
    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType RangeType;
    typedef typename BaseType::DerivativeType DerivativeType;

    typedef GridViewType GridView;
    typedef typename BaseType::Grid Grid;

    //! Element type of underlying grid
    typedef typename Grid::template Codim<0>::Entity Element;

    /**
     *  \brief Constructor
     *
     *  We need to know the underlying grid view
     */
    VirtualGridViewFunction(const GridView& gridView) :
      BaseType(gridView.grid()),
      gridView_(gridView)
    {}

    /**
     * \brief function evaluation in global coordinates
     *
     * Default implementation using hierarchic search and evaluateLocal
     *
     * \warning may be VERY SLOW due to hierarchic search
     *
     * \param x Argument (global coordinates) for function evaluation
     * \param y Result of function evaluation.
     */
    virtual void evaluate(const DomainType& x, RangeType& y) const
    {
      Dune::HierarchicSearch<Grid, GridView> hsearch(gridView_.grid(), gridView_);

      auto e = hsearch.findEntity(x);
      evaluateLocal(e, e.geometry().local(x), y);
    }

    /** \brief evaluation of 1st derivative in global coordinates
     *
     * Default implementation using hierarchic search and evaluateDerivativeLocal
     *
     * \warning may be VERY SLOW due to hierarchical search
     *
     *  \param x point (global coordinates) at which to evaluate the derivative
     *  \param d will contain the derivative at x after return
     */
    virtual void evaluateDerivative(const DomainType& x, DerivativeType& d) const
    {
      Dune::HierarchicSearch<Grid, GridView> hsearch(gridView_.grid(), gridView_);

      auto e = hsearch.findEntity(x);
      evaluateDerivativeLocal(e, e.geometry().local(x), d);
    }


    /**
     * \brief Function evaluation in local coordinates.
     *
     * \param e Evaluate in local coordinates with respect to this element.
     * \param x Argument for function evaluation in local coordinates.
     * \param y Result of function evaluation.
     */
    virtual void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const = 0;


    /** \brief evaluation of derivative in local coordinates
     *
     *  \param e Evaluate in local coordinates with respect to this element.
     *  \param x point in local coordinates at which to evaluate the derivative
     *  \param d will contain the derivative at x after return
     */
    virtual void evaluateDerivativeLocal(const Element& e, const LocalDomainType& x, DerivativeType& d) const
    {
      DUNE_THROW(Dune::NotImplemented, "Derivative not implemented");
    }


    /**
     * \brief Check whether local evaluation is possible
     *
     * \param e Return true if local evaluation is possible on this element.
     */
    virtual bool isDefinedOn(const Element& e) const
    {
      return gridView_.contains(e);
    }

    virtual ~VirtualGridViewFunction() {}

    /**
     * \brief Export underlying grid view
     */
    const GridView& gridView() const
    {
      return gridView_;
    }

//    postponed!
//    //! Raw type of input variable in local coordinates with removed reference and constness
//    typedef typename Grid::template Codim<0>::Geometry::LocalCoordinate LocalDomainType;
//    /**
//     * \brief Create a local representation of this function on given element.
//     *
//     * \param e The local function takes coordinates with respect to this element.
//     */
//    virtual LocalFunction local(const Element& e) const
//    {
//      return LocalGridFunction<typename GridView::Grid, MyType>(*this, e);
//    }


  protected:

    const GridView gridView_;

}; // end of VirtualGridViewFunction class

// postponed!
///**
// * \brief Wrapper to represent local a grid function in local coordinates
// *
// */
//template<class GridType, class GridFunctionImplementation>
//class LocalGridFunction
//  public Dune::VirtualFunction<
//    typename GridFunctionImplementation::LocalDomainType,
//    typename GridFunctionImplementation::RangeType>
//{
//  protected:

//    typedef typename Dune::template VirtualFunction<
//      typename GridFunctionImplementation::LocalDomainType,
//      typename GridFunctionImplementation::RangeType>
//        BaseType;

//  public:

//    typedef typename BaseType::DomainType DomainType;
//    typedef typename BaseType::RangeType RangeType;

//    /** \brief Element type of underlying grid */
//    typedef typename GridType::template Codim<0>::Entity Element;

//    LocalGridFunction(const GridFunctionImplementation& f, const Element& e)
//      f_(f),
//      e_(e)
//    {}

//    /**
//     * \brief Function evaluation.
//     *
//     * \param x Argument for function evaluation.
//     * \param y Result of function evaluation.
//     */
//    virtual void evaluate(const DomainType& x, RangeType& y) const
//    {
//      f_.GridFunctionImplementation::evaluateLocal(*e, x, y);
//    }

//  protected:

//    const GridFunctionImplementation& f_;
//    const Element& e_;

//};


#endif
