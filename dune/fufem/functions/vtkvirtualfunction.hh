// -*- c-basic-offset:4 tab-width:4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef VTK_VIRTUAL_FUNCTION_HH
#define VTK_VIRTUAL_FUNCTION_HH

#include <memory>

#include <dune/grid/io/file/vtk/function.hh>

#include <dune/fufem/functions/analyticgridfunction.hh>

/** \brief A VTK function-wrapper for Dune::VirtualFunctions.
 *
 *  This class wraps a Dune::VirtualFunction as a VTKFunction in order to put it into say the VTKWriter.
 *  If VirtualFunctionType is a VirtualGridFunction (dune-fufem) the evaluateLocal method is taken advantage of,
 *  otherwise it is in turn wrapped into an AnalyticGridFunction via the static makeGridFunction method.
 *  \tparam GV the type of the GridView on which the VTKFunction shall live
 *  \tparam VirtualFunctionType the type of the function to be wrapped
*/
template<class GV, class VirtualFunctionType>
class VTKVirtualFunction : public Dune::VTKFunction<GV>
{
    typedef Dune::VTKFunction<GV> Base;
    typedef typename  VirtualFunctionType::DomainType DomainType;
    typedef typename  VirtualFunctionType::RangeType  RangeType;
    typedef typename GV::Grid GridType;
    typedef VirtualGridFunction<GridType, RangeType> GF;
public:
    typedef typename Base::Entity Entity;
    typedef typename Base::ctype ctype;
    using Base::dim;

    /** \brief Construct from given gridview, VirtualFunction, and name.
     *
     *  \param gridview the gridview on which the VTKFunction shall live
     *  \param f        the function to be wrapped
     *  \param s        a name of the function.
     */
    VTKVirtualFunction(const GV& gridview, const VirtualFunctionType& f, const std::string& s) :
        gridview_(gridview),
        wrappedFunction_(makeGridFunction(gridview.grid(),f)),
        s_( s )
    {
    }

    /** \brief Get the number of components the function has. */
    virtual int ncomps () const
    {
        return RangeType::dimension;
    }

    /** \brief Locally evaluate a component of the function.
     *
     *  \param comp the component to evaluate.
     *  \param e    the element the local coordinates are taken from.
     *  \param xi   the local coordinates where to evaluate the function.
     */
    virtual double evaluate (int comp, const Entity &e,
                             const DomainType &xi) const
    {
        RangeType y;
        wrappedFunction_->evaluateLocal(e,xi,y);

        return y[comp];
    }

    /** \brief Get the name of that function. */
    virtual std::string name () const
    {
        return s_;
    }

    /** \brief Destructor. */
    virtual ~VTKVirtualFunction() {}

private:
    const GV& gridview_;
    const std::shared_ptr<const GF> wrappedFunction_;
    std::string s_;
};
#endif
