#ifndef DG_PQKBASIS_HH
#define DG_PQKBASIS_HH

/*
#warning This file is deprecated.  All implementations of function space bases in dune-fufem \
  are in the process of being replaced by counterparts in the new dune-functions module. \
  Those are syntactically different, but semantically very close to the dune-fufem implementations. \
  To get rid of this warning, replace all occurrences of the Q1NodalBasis<...> class in your code \
  by DuneFunctionsBasis<Dune::Functions::LagrangeDGBasis<...> >.
*/

/**
    \brief Global basis for discontinous Galerkin spaces with local Pk or Qk elements
 */

#include <dune/localfunctions/lagrange/pqkfactory.hh>

#include <dune/fufem/functionspacebases/functionspacebasis.hh>

#include <dune/fufem/dgpqkindexset.hh>


template <class GV, class RT=double, int order=1>
class DgPQKBasis :
    public FunctionSpaceBasis<
        GV,
        RT,
        typename Dune::PQkLocalFiniteElementCache<typename GV::Grid::ctype, RT, GV::dimension, order>::FiniteElementType >
{
    protected:
        typedef typename GV::Grid::ctype ctype;

        typedef typename Dune::PQkLocalFiniteElementCache<typename GV::Grid::ctype, RT, GV::dimension, order> FiniteElementCache;
        typedef typename FiniteElementCache::FiniteElementType LFE;


        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;

        using Base::dim;
        using Base::gridview_;


    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::LinearCombination LinearCombination;

        DgPQKBasis(const GridView& gridview) :
            Base(gridview),
            dgIndexSet_(gridview)
        {}

        size_t size() const
        {
            return dgIndexSet_.size();
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const
        {
            return cache_.get(e.type());
        }

        int index(const Element& e, const int i) const
        {
            return dgIndexSet_(e)+i;
        }

    protected:
        FiniteElementCache cache_;
        DGPQKIndexSet<GV,order> dgIndexSet_;
};

#endif

