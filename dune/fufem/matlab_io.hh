#ifndef MATLAB_IO_HH
#define MATLAB_IO_HH

#include <array>
#include <cstdio>
#include <string>

#include <dune/common/exceptions.hh>

/** \file
    \brief Code for writing 1d-grid data in Matlab format
*/

/** \brief Open file for writing and write the coordinates of the
    grid vertices.  

    We implicitly assume that the grid is 1D.
*/
template<class GridType>
FILE* openFile(const std::string& filename, GridType& grid)
{
    enum {dim = GridType::dimension};

    // Open file
    FILE* fp = fopen(filename.c_str(), "w");
  
    typedef typename GridType::template Codim<dim>::LevelIterator VertexIterator;
    VertexIterator vit    = grid.template lbegin<dim>(grid.maxLevel());
    VertexIterator vendit = grid.template lend<dim>(grid.maxLevel());

    for (; vit!=vendit; ++vit)
        fprintf(fp, "%g ", vit->geometry().corner(0)[0]);

    fprintf(fp,"\n");

    return fp;  
}



/** \brief Open file for writing and write the coordinates of the
    grid vertices FOR TWO GRIDS.  

    This is for domain decomposition stuff. We implicitly assume that the grids are 1D.
*/
template<class GridType>
FILE* openFileDD(const std::string& filename, const std::array<GridType*, 2>& grid)
{
    enum {dim = GridType::dimension};

    // Open file
    FILE* fp = fopen(filename.c_str(), "w");

    typedef typename GridType::template Codim<dim>::LevelIterator VertexIterator;

    // Loop over the subgrids
    for (int i=0; i<2; i++) {

        VertexIterator vit    = grid[i]->template lbegin<dim>(grid[i]->maxLevel());
        VertexIterator vendit = grid[i]->template lend<dim>(grid[i]->maxLevel());
        
        for (; vit!=vendit; ++vit) {
            
            fprintf(fp, "%g ", vit->geometry().corner(0)[0]);
            
        }

    }

    fprintf(fp,"\n");

    return fp;  
}



/** \brief Append the content of v as one ascii line to fp
 */
template<class VectorType>
void write(FILE* fp, const VectorType& u)
{
    typedef typename VectorType::ConstIterator ConstIterator;
    ConstIterator uIt    = u.begin();
    ConstIterator uEndIt = u.end();

    for (; uIt!=uEndIt; ++uIt) 
        fprintf(fp, "%g ", (*uIt)[0]);
    
    fprintf(fp, "\n");
}

/** \brief Append the content of v as one ascii line to fp tracking two subdomains
 */
template<class VectorType>
void write(FILE* fp, const VectorType& u, int sd)
{
    // The subDomain which needs to be written next
    // (due to the file format the two domains have to be written alternatingly)
    static int subDomain = 0;

    if (subDomain!=sd)
        DUNE_THROW(Dune::IOError, "You sent subdomain " << sd << ", but subdomain " << subDomain
                   << "is scheduled next!");

    typedef typename VectorType::ConstIterator Iterator;
    Iterator uIt    = u.begin();
    Iterator uEndIt = u.end();

    for (; uIt!=uEndIt; ++uIt) 
        fprintf(fp, "%3.15e ", (*uIt)[0]);

    if (subDomain==1)
        fprintf(fp, "\n");

    // alternate between two subdomains
    subDomain = (subDomain+1)%2;
}


#endif

