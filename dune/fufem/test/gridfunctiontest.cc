#include <config.h>

#include <cmath>
#include <cstdio>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/functions/virtualdifferentiablefunction.hh>

template <class GridType>
class TestGridFunction:
    public VirtualGridFunction<GridType, double>
{
        typedef VirtualGridFunction<GridType, double> BaseType;
        typedef typename GridType::template Codim<0>::Entity Element;

    public:
        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;
        typedef typename BaseType::DerivativeType DerivativeType;

        TestGridFunction(GridType& grid):
            VirtualGridFunction<GridType, double>(grid)
        {}


        virtual void evaluateLocal(const Element& e, const DomainType& x, RangeType& y) const
        {
            passed = passed and isDefinedOn(e);
            y=0.0;
            DomainType xg = e.geometry().global(x);

            for (int i=0; i<GridType::dimension; ++i)
                y += sin(xg[i]);
        }

        virtual void evaluateDerivativeLocal(const Element& e, const DomainType& x, DerivativeType& d) const
        {
            passed = passed and isDefinedOn(e);
            d=0.0;
            DomainType xg = e.geometry().global(x);

            for (int i=0; i<GridType::dimension; ++i)
                d[0][i] = cos(xg[i]);
        }

        virtual bool isDefinedOn(const Element& e) const
        {
            typename GridType::ctype xmax=0.0;
            for (int i=0; i<e.geometry().corners(); ++i)
                xmax = std::max(xmax,std::abs(e.geometry().corner(i)[0]));

            return (xmax<=0.5 and e.isLeaf());
        }
  bool mutable passed = true;
};

template <class GridViewType>
class TestGridViewFunction:
    public VirtualGridViewFunction<GridViewType,double>
{
        typedef VirtualGridViewFunction<GridViewType, double> BaseType;
        typedef typename GridViewType::template Codim<0>::Entity Element;

    public:
        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;
        typedef typename BaseType::DerivativeType DerivativeType;

        TestGridViewFunction(GridViewType& gridview):
            VirtualGridViewFunction<GridViewType, double>(gridview)
        {}

        virtual void evaluateLocal(const Element& e, const DomainType& x, RangeType& y) const
        {
            passed = passed and this->isDefinedOn(e);
            y=0.0;
            DomainType xg = e.geometry().global(x);

            for (int i=0; i<GridViewType::dimension; ++i)
                y += sin(xg[i]);
        }

        virtual void evaluateDerivativeLocal(const Element& e, const DomainType& x, DerivativeType& d) const
        {
            passed = passed and this->isDefinedOn(e);
            d=0.0;
            DomainType xg = e.geometry().global(x);

            for (int i=0; i<GridViewType::dimension; ++i)
                d[0][i] = cos(xg[i]);
        }
  bool mutable passed = true;
};

int main(int argc, char** argv)
{
    bool passed = true;
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is GridFunctionTest v0.01" << std::endl;

    /* Tests for correct DerivativeTypes */
    {
        typedef double rctype;  // range field type
        typedef double dctype;  // domain field type
        const int ddim = 2;     // domain dimension
        const int rdim = 3;     // range dimension

        typedef Dune::FieldVector<dctype,ddim> DomainType;
        typedef Dune::FieldVector<rctype,rdim> RangeType;

        // expected types: only for these we're testing, so if you need other Domain and RangeTypes (e.g. matrix-valued functions) add the test
        //                 and the corresponding template specialization to virtualdifferentiablefunction.hh

        // expected DerivativeType for DomainType=Dune::FieldVector<dctype,ddim>, RangeType=double
        //                             DomainType=Dune::FieldVector<dctype,ddim>, RangeType=FieldVector<rctype,1>
        typedef Dune::FieldMatrix<rctype,1,ddim>    DType4ScalarFct;

        // expected DerivativeType for DomainType=Dune::FieldVector<dctype,ddim>, RangeType=FieldVector<rctype,rdim>
        typedef Dune::FieldMatrix<rctype,rdim,ddim> DType4VectorFct;

        static_assert(std::is_same<DerivativeTypefier<DomainType, double>::DerivativeType, DType4ScalarFct>::value,
                "Wrong derivative type for scalar function (DomainType=Dune::FieldVector<dctype,ddim>, RangeType=double) ");
        static_assert(std::is_same<DerivativeTypefier<DomainType, Dune::FieldVector<rctype,1> >::DerivativeType, DType4ScalarFct>::value,
                "Wrong derivative type for scalar function (DomainType=Dune::FieldVector<dctype,ddim>, RangeType=FieldVector<rctype,1>) ");
        static_assert(std::is_same<DerivativeTypefier<DomainType, RangeType>::DerivativeType, DType4VectorFct>::value,
                "Wrong derivative type for vector function (DomainType=Dune::FieldVector<dctype,ddim>, RangeType=FieldVector<rctype,rdim>)");
    }

    // the underlying grid for GridFunction and GridViewFunction tests
    typedef Dune::YaspGrid<2> GridType;
    typedef GridType::Codim<0>::LeafIterator ElementIterator;
    typedef GridType::Codim<0>::Geometry::LocalCoordinate LocalCoords;

    Dune::FieldVector<double,2> L(1.0);
    std::array<int,2> s = {{1, 1}};

    GridType grid(L,s);

    grid.globalRefine(1);
    grid.globalRefine(1);

    /* Tests for VirtualGridFunction */
    /* testing the default implementation of evaluate resp evaluateDerivative */
    {
        TestGridFunction<GridType> testgridfunction(grid);

        ElementIterator elt = grid.leafbegin<0>();
        ElementIterator eltEnd = grid.leafend<0>();
        for (; elt!=eltEnd; ++elt)
        {
            LocalCoords xlocal(0.0);
            for (std::size_t d=0; d<xlocal.N(); ++d)
                xlocal[d] = (1.0*rand())/RAND_MAX;

            TestGridFunction<GridType>::RangeType flocal, fglobal;
            TestGridFunction<GridType>::DerivativeType dlocal, dglobal, ddiff;

            if (testgridfunction.isDefinedOn(*elt))
            {
                testgridfunction.evaluateLocal(*elt,xlocal,flocal);
                testgridfunction.evaluate(elt->geometry().global(xlocal),fglobal);

                testgridfunction.evaluateDerivativeLocal(*elt,xlocal,dlocal);
                testgridfunction.evaluateDerivative(elt->geometry().global(xlocal),dglobal);

                ddiff = dlocal;
                ddiff -= dglobal;
                if (ddiff[0].two_norm()>1e-13)
                {
                    std::cout << "dlocal= " << dlocal << ", dglobal= " << dglobal << std::endl;
                    DUNE_THROW(Dune::Exception,"Default global evaluation of derivative returns wrong result.");
                }
                if (std::abs(flocal-fglobal)>1e-13)
                {
                    std::cout << "flocal= " << flocal << ", fglobal= " << fglobal << std::endl;
                    DUNE_THROW(Dune::Exception,"Default global evaluation returns wrong result.");
                }
            }
        }
        passed = passed and testgridfunction.passed;
    }

    /* Tests for VirtualGridViewFunction */
    /* testing the default implementations of evaluate resp evaluateDerivative and isDefinedOn for LeafGridView and LevelGridView */
    {
        typedef GridType::LeafGridView LeafGridView;
        typedef GridType::LevelGridView LevelGridView;
        typedef LevelGridView::Codim<0>::Iterator LevelElementIterator;

        const int LEVEL = 1;

        LeafGridView leafview=grid.leafGridView();
        LevelGridView levelview=grid.levelGridView(LEVEL);

        TestGridViewFunction<LeafGridView> testleafgridviewfunction(leafview);
        TestGridViewFunction<LevelGridView> testlevelgridviewfunction(levelview);

        TestGridViewFunction<LeafGridView>::RangeType flocal, fglobal;
        TestGridViewFunction<LeafGridView>::DerivativeType dlocal, dglobal, ddiff;
        TestGridViewFunction<LevelGridView>::RangeType fflocal, ffglobal;
        TestGridViewFunction<LevelGridView>::DerivativeType ddlocal, ddglobal, dddiff;

        for (int level=0; level<grid.maxLevel(); ++level)
        {
            LevelElementIterator elt = grid.lbegin<0>(level);
            LevelElementIterator eltEnd = grid.lend<0>(level);
            for (; elt!=eltEnd; ++elt)
            {
                LocalCoords xlocal(0.0);
                for (std::size_t d=0; d<xlocal.N(); ++d)
                    xlocal[d] = (1.0*rand())/RAND_MAX;

                if (testleafgridviewfunction.isDefinedOn(*elt))
                {
                    // test isDefinedOn default implementation
                    passed = passed and elt->isLeaf();

                    // test evaluate evaluateDerivative default implementations
                    testleafgridviewfunction.evaluateLocal(*elt,xlocal,flocal);
                    testleafgridviewfunction.evaluate(elt->geometry().global(xlocal),fglobal);

                    testleafgridviewfunction.evaluateDerivativeLocal(*elt,xlocal,dlocal);
                    testleafgridviewfunction.evaluateDerivative(elt->geometry().global(xlocal),dglobal);

                    ddiff = dlocal;
                    ddiff -= dglobal;
                    if (ddiff[0].two_norm()>1e-13)
                    {
                        std::cout << "dlocal= " << dlocal << ", dglobal= " << dglobal << std::endl;
                        DUNE_THROW(Dune::Exception,"Default global evaluation of derivative returns wrong result.");
                    }
                    if (std::abs(flocal-fglobal)>1e-13)
                    {
                        std::cout << "flocal= " << flocal << ", fglobal= " << fglobal << std::endl;
                        DUNE_THROW(Dune::Exception,"Default global evaluation returns wrong result.");
                    }
                }
                else
                    // test isDefinedOn default implementation
                    passed = passed and not(elt->isLeaf());

                if (testlevelgridviewfunction.isDefinedOn(*elt))
                {
                    // test isDefinedOn default implementation
                    passed = passed and elt->level()==LEVEL;

                    // test evaluate evaluateDerivative default implementations
                    testlevelgridviewfunction.evaluateLocal(*elt,xlocal,fflocal);
                    testlevelgridviewfunction.evaluate(elt->geometry().global(xlocal),ffglobal);

                    testlevelgridviewfunction.evaluateDerivativeLocal(*elt,xlocal,ddlocal);
                    testlevelgridviewfunction.evaluateDerivative(elt->geometry().global(xlocal),ddglobal);

                    dddiff = ddlocal;
                    dddiff -= ddglobal;
                    if (dddiff[0].two_norm()>1e-13)
                    {
                        std::cout << "ddlocal= " << ddlocal << ", ddglobal= " << ddglobal << std::endl;
                        DUNE_THROW(Dune::Exception,"Default global evaluation of derivative returns wrong result.");
                    }
                    if (std::abs(fflocal-ffglobal)>1e-13)
                    {
                        std::cout << "fflocal= " << fflocal << ", ffglobal= " << ffglobal << std::endl;
                        DUNE_THROW(Dune::Exception,"Default global evaluation returns wrong result.");
                    }
                }
                else
                    // test isDefinedOn default implementation
                    passed = passed and elt->level()!=LEVEL;

            }
        }
        passed = passed and testlevelgridviewfunction.passed;
    }

    /* Tests for VirtualGridViewFunction */

    return passed ? 0 : 1;
}
